//Generate a random integer, inclusive
function randInt(min, max){
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function boxBoxCollision(b1x, b1y, b1w, b1h, b2x, b2y, b2w, b2h){
	if(b1x+b1w < b2x || b1x > b2x+b2w)
		return false;
	if(b1y+b1h < b2y || b1y > b2y+b2h)
		return false;
	return true;
}
var game = new Phaser.Game(800, 600, Phaser.AUTO, 'Cat Clicker', { preload: preload, create: create, update: update });

var map;
var ai;

const WORLD_WIDTH = 800/25;
const WORLD_HEIGHT = 600/25;

function preload(){
	game.load.image('stone', 'img/stoneTile.png');
	game.load.image('wood', 'img/woodTile.png');
}

function create(){

	//Generate a new map
	map = new MAP();
	map.create();

	//Generate a new AI
	ai = new AI();
	ai.create();

	drawWorld(map.world);	
}

function update(){
	//Update the game world
	map.update();

	//update the ai player
	ai.update();
}

function drawWorld(world){
	for(var i = 0; i < WORLD_HEIGHT; ++i){
		for(var j = 0; j < WORLD_WIDTH; ++j){
			switch(world[i][j]){
				case 0:				
					
					break;
				case 1:
					var t = game.add.sprite(j * 25, i * 25, 'stone');
					t.anchor.set(0);
					break;
				case 2:
					var t = game.add.sprite(j * 25, i * 25, 'wood');
					t.anchor.set(0);
					break;
				default:
					console.log("default");
					break;
			}
		}
	}
}
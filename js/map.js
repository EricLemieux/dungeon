function MAP(){
	this.create = function(){
		//Generate the blank map
		for(var i = 0; i < WORLD_HEIGHT; ++i){
			var data = [];
			for(var j = 0; j < WORLD_WIDTH; ++j){
				data.push(0);
			}
			this.world.push(data);
		}

		var maxNumRooms = randInt(3,10);
		var rooms = [];
		rooms.push([0,0,3,3]);

		while(this.world[WORLD_HEIGHT-1][WORLD_WIDTH-1] != 1){
			var x,y,w,h;
			var colliding = false;
			var llr = rooms[rooms.length-1];

			//Generate a room that isnt connected
			do{
				x = randInt(0, WORLD_WIDTH-1);
				y = randInt(0, WORLD_HEIGHT-1);
				w = randInt(2,5);
				h = randInt(2,5);
			}while(boxBoxCollision(x,y,w,h,llr[0],llr[1],llr[2],llr[3]));

			this.world[y][x] = 1;
			
			rooms.push([x,y,w,h]);
			alert("beep");


			var lr = rooms[rooms.length-1];

			//now generate a room that connects the two of them
			do{
				x = randInt(0, WORLD_WIDTH-1);
				y = randInt(0, WORLD_HEIGHT-1);
				w = randInt(1,5);
				h = randInt(1,5);
			}while(!boxBoxCollision(x,y,w,h,lr[0],lr[1],lr[2],lr[3]) && !boxBoxCollision(x,y,w,h,llr[0],llr[1],llr[2],llr[3]));

			this.world[y][x] = 1;
			
			rooms.push([x,y,w,h]);
		}

		//create the rooms
		for(var i = 0; i < rooms.length; ++i){
			for(var a = -1*rooms[i][2]; a < rooms[i][2]; ++a){
				for(var b = -1*rooms[i][3]; b < rooms[i][3]; ++b){
					var y = Math.max(0,Math.min(rooms[i][0] + b,WORLD_HEIGHT-1));
					var x = Math.max(0,Math.min(rooms[i][1] + a,WORLD_WIDTH-1));
					this.world[y][x] = 1;
				}
			}
		}
	}
	this.update = function(){
		//TODO
	}

	this.world = [];
}